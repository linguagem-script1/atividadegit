#!/bin/bash



# Exibir informações da CPU
echo "===== Informações da CPU ====="
lscpu

# Exibir informações da memória
echo "===== Informações da Memória ====="
free -h

# Exibir informações de armazenamento
echo "===== Informações de Armazenamento ====="
lsblk

# Exibir informações da placa-mãe
echo "===== Informações da Placa-Mãe ====="
dmidecode -t baseboard

# Exibir informações dos dispositivos PCI
echo "===== Dispositivos PCI ====="
lspci

# Exibir informações da placa de vídeo
echo "===== Informações da Placa de Vídeo ====="
lspci | grep -i vga

# Exibir informações do disco rígido
echo "===== Informações do Disco Rígido ====="
hdparm -I /dev/sda  # Substitua /dev/sda pelo dispositivo de disco rígido adequado

# Exibir informações da rede
echo "===== Informações de Rede ====="
ifconfig -a
