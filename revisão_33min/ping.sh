#!/bin/bash

while true; do
   # Exibe o menu
   echo "Selecione uma opção:"
   echo "a - Executar um ping"
   echo "b - Exibir todos os usuários logados"
   echo "c - Exibir o uso da memória e de disco"
   echo "d - Encerrar o programa"

   # Lê a entrada do usuário
   read -p "Opção: " opcao

   # Verifica a opção selecionada
   if [ "$opcao" == "a" ]; then
      echo "Digite o IP ou site para pingar:"
      read -p "IP ou site: " ip
      ping -c 5 $ip
   elif [ "$opcao" == "b" ]; then
      who
   elif [ "$opcao" == "c" ]; then
      df -h
      free
   elif [ "$opcao" == "d" ]; then
      exit
   else
      echo "Opção inválida."
   fi
done
