#!/bin/bash


echo 'Não Sei – Cora Coralina'
sleep 3
echo -e "\nNão sei se a vida é curta ou longa para nós,"
sleep 3
echo 'mas sei que nada do que vivemos tem sentido,'
sleep 3
echo 'se não tocarmos o coração das pessoas.'
sleep 3
echo 'Muitas vezes basta ser: colo que acolhe,'
sleep 3
echo 'braço que envolve, palavra que conforta,'
sleep 3
echo 'silencio que respeita, alegria que contagia,'
sleep 3
echo 'lágrima que corre, olhar que acaricia,'
sleep 3
echo 'desejo que sacia, amor que promove.'
sleep 3
echo 'E isso não é coisa de outro mundo,'
sleep 3
echo 'é o que dá sentido à vida.'
sleep 3
echo 'É o que faz com que ela não seja nem curta,'
sleep 3
echo 'nem longa demais, mas que seja intensa,'
sleep 3
echo 'verdadeira, pura enquanto durar.'
sleep 3
echo 'Feliz aquele que transfere o que sabe'
sleep 3
echo 'e aprende o que ensina”.'
