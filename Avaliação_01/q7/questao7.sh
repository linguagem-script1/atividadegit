#!/bin/bash

# Verificar se foram fornecidos exatamente 3 argumentos
[ $# -ne 3 ] && echo "Uso: $0 num1 num2 num3" && exit 1

# Calcular a soma usando o comando 'bc'
soma=$(echo "$1 + $2 + $3" | bc)

# Imprimir o resultado da soma
echo "A soma de $1, $2 e $3 é igual a $soma"
