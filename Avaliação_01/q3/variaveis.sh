#!/bin/bash
echo -e "\n"
echo -e "Atribuição Direta: A forma mais simples de criar uma variável é atribuir um \nvalor a ela usando o operador =. Não deve haver espaços em torno do =."
echo -e "\nEx: v1 = 'Esta é uma variável com conteúdo fixo'"
echo -e "\n"

echo -e "Atribuição de Comando: Você pode criar uma variável atribuindo o resultado \nde um comando a ela usando a substituição de comando com $() ou crase (backticks)."
echo -e "\n"
echo 'Ex: dt=$(date)'
echo dt=$(date)
echo -e "\n"

echo -e "Variáveis de Ambiente: Você pode criar variáveis de ambiente definindo-as \nantes de executar um comando. Isso faz com que a variável esteja disponível \npara o comando e todos os seus subprocessos."
echo -e "\n"
echo 'Ex: va=valor da variável de ambiente'
echo 'export va'
echo -e "\n"

echo -e "Variáveis em Linha de Comando: Você pode criar variáveis ao executar um \nscript ou comando, passando argumentos na linha de comando. \nEssas variáveis são acessadas como" '$1, $2, etc., onde $1' "representa o \nprimeiro argumento passado, $2 o segundo e assim por diante."
echo -e "\n"
echo -e "Na linha de comando: ./meuscript.sh arg1 arg2 \nDentro do script meuscript.sh:\nprimeiro_arg='$'1'\nsegundo_arg='$'2'"
echo -e "\n" 

echo "Variáveis de Leitura do Usuário: Você pode criar variáveis ao ler valores diretamente do usuário usando o comando read."
echo -e "Ex:echo Digite seu nome:\nread -p nome" 
echo -e "\n"

echo "Pedir explicitamente para o usuário digitar valores é útil quando você precisa de entrada interativa e flexibilidade, enquanto receber variáveis como parâmetros de linha de comando é mais adequado para scripts automatizados e situações em que os valores são conhecidos antecipadamente. A escolha entre essas abordagens depende da natureza do seu script e dos requisitos específicos da sua aplicação."
echo -e "\n"

echo -e "Variáveis Automáticas: O Bash possui várias variáveis automáticas que são \npredefinidas e contêm informações sobre o ambiente e a execução do script."
echo -e "\n"
echo "Exemplos de variáveis automáticas:"
echo "Nome do script: \$0 = $0"
echo "Número de argumentos: \$# = $#"
echo "Todos os argumentos separados por espaço: \$* = $*"
echo "Todos os argumentos separados por espaço (com aspas): \$@ = $@"
