#!/bin/bash

# Verificar se foram fornecidos exatamente 4 argumentos
[ $# -ne 4 ] && { echo "Uso: $0 nome1 nome2 nome3 nome4"; exit 1; }

# Primeiro nome
mkdir -p "$1" && data=$(date +"%d/%m/%Y") && echo "# $1" > "$1/README.md" && echo "Data: $data" >> "$1/README.md" && echo "Diretório '$1' criado com sucesso!" || echo "Erro ao criar diretório '$1'"

# Segundo nome
mkdir -p "$2" && data=$(date +"%d/%m/%Y") && echo "# $2" > "$2/README.md" && echo "Data: $data" >> "$2/README.md" && echo "Diretório '$2' criado com sucesso!" || echo "Erro ao criar diretório '$2'"

# Terceiro nome
mkdir -p "$3" && data=$(date +"%d/%m/%Y") && echo "# $3" > "$3/README.md" && echo "Data: $data" >> "$3/README.md" && echo "Diretório '$3' criado com sucesso!" || echo "Erro ao criar diretório '$3'"

# Quarto nome
mkdir -p "$4" && data=$(date +"%d/%m/%Y") && echo "# $4" > "$4/README.md" && echo "Data: $data" >> "$4/README.md" && echo "Diretório '$4' criado com sucesso!" || echo "Erro ao criar diretório '$4'"
