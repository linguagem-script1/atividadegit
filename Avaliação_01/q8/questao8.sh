#!/bin/bash

# Verificar se foram fornecidos exatamente 3 argumentos
[ $# -ne 3 ] && { echo "Uso: $0 arquivo1 arquivo2 arquivo3"; exit 1; }

# Extrair o número de linhas de cada arquivo e somar
soma=$(( $(wc -l < "$1") + $(wc -l < "$2") + $(wc -l < "$3") ))

# Imprimir a soma
echo "A soma dos números de linhas dos arquivos é: $soma"
