#!/bin/bash

if [ -e "ips.log" ]; then

    # Passo 3: Listar todos os IPs únicos do arquivo
    separar_ips() {
        grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" ips.log | sort -u
    }
    ips=$(separar_ips)
    
    # Passo 4: Realizar um ping para cada IP único
    for ip in $ips; do
        ping -c 1 $ip 2>> /dev/null
        if [ $? -eq 0 ]; then
            echo "Ping para $ip foi bem-sucedido."
        else
            echo "O ping para $ip falhou."
        fi
    done

else
    
    wget https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log
    
    # Passo 1: Criar um novo arquivo com as 1000 primeiras e últimas linhas
    head -n 1000 access.log > ips.log
    tail -n 1000 access.log >> ips.log
    
    # Passo 2: Remover o arquivo access.log
    rm access.log
fi
