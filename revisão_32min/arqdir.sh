#!/bin/bash

# Verifica se o script está sendo executado como usuário ifpb.

if [ "$(whoami)" != "ifpb" ]; then
  echo "Este script deve ser executado como usuário ifpb."
  exit 1
fi

# Verifica se foi passado algum argumento de linha de comando.

if [ -z "$1" ]; then
  dir="/home/ifpb"
else
  dir="$1"
fi

# Conta os arquivos e diretórios no diretório especificado.

files=$(find "$dir" -type f | wc -l)
directories=$(find "$dir" -type d | wc -l)

# Exibe as informações solicitadas.

echo "Arquivos: $files"
echo "Diretórios: $directories"
