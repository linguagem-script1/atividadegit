#!/bin/bash

az="" #Nome do arquivo zip
da=$(pwd) # Diretório atual

while true; do
    clear
    echo "Menu:"
    echo "1. Escolher ou criar um arquivo .zip"
    echo "2. Listar o conteúdo do arquivo .zip"
    echo "3. Pré-visualizar um arquivo no .zip(Precione 'q' para sair da vizualização)"
    echo "4. Adicionar arquivos ao .zip"
    echo "5. Remover arquivos do .zip"
    echo "6. Extrair todo o conteúdo do .zip"
    echo "7. Extrair arquivos específicos do .zip"
    echo "8. Sair"

    read -p "Escolha uma opção: " op # Variável que quarda opção

    if [ "$op" == "1" ]; then
        read -p "Digite o nome do arquivo .zip (ou digite um novo nome para criar): " az
        if [ -f "$az" ]; then
            echo "Arquivo .zip existente selecionado: $az"
        else
            read -p "Digite o nome do arquivo a ser compactado (no diretório atual): " ac # Variável nome do arquivo a ser compactado
            if [ -f "$ac" ]; then
                echo "Criando um novo arquivo .zip: $az"
                zip -q "$az" "$ac"
                echo "Arquivo .zip criado com sucesso."
            else
                echo "O arquivo a ser compactado não existe no diretório atual."
            fi
        fi
    elif [ "$op" == "2" ]; then
        if [ -n "$az" ]; then
            unzip -l "$az"
            read -p "Pressione Enter para continuar..."
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "3" ]; then
        if [ -n "$az" ]; then
            read -p "Digite o nome do arquivo para pré-visualizar: " va #variável nome do arquivo a ser vizualizado.
            unzip -p "$az" "$va" | less
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "4" ]; then
        if [ -n "$az" ]; then
            read -p "Digite o nome do arquivo a ser adicionado: " ad # Variável nome do arquivo para adicionar
            zip -q "$az" "$ad"
            echo "Arquivo adicionado ao .zip com sucesso."
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "5" ]; then
        if [ -n "$az" ]; then
            read -p "Digite o nome do arquivo a ser removido: " ra # Variável nome do arquivo a ser removido.
            zip -q "$az" -d "$ra"
            echo "Arquivo removido do .zip com sucesso."
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "6" ]; then
        if [ -n "$az" ]; then
            unzip  "$az"
            echo "Conteúdo do .zip extraído com sucesso."
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "7" ]; then
        if [ -n "$az" ]; then
            read -p "Digite o nome do arquivo a ser extraído: " ea # Variável nome do arquivo a ser extraido.
            unzip  "$az" "$ea"
            echo "Arquivo extraído do .zip com sucesso."
        else
            echo "Nenhum arquivo .zip selecionado. Escolha a opção 1 para criar ou selecionar um arquivo .zip."
            read -p "Pressione Enter para continuar..."
        fi
    elif [ "$op" == "8" ]; then
        echo "Saindo..."
        exit
    else
        echo "Opção inválida. Tente novamente."
        read -p "Pressione Enter para continuar..."
    fi
done
