#!/bin/bash
source func.sh

# Aplica a customização escolhida
apply_customization() {
    set_prompt "$1" "$2" "$3"
    echo "Configuração aplicada."
}

# Utiliza os argumentos fornecidos para aplicar a customização
apply_customization "$1" "$2" "$3"
