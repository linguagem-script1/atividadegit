#!/bin/bash

# Função para configurar o PS1 com as opções escolhidas
set_prompt() {
    local cor="$1"
    local usuario="$2"
    local dlinhas="$3"
    local smb="$4"

    # Configuração padrão do PS1
    PS1="\[$(tput setaf $cor)\]"

    if [ "$usuario" = true ]; then
        PS1+="\u"
    else
        PS1+="\u@\h"
    fi

    if [ "$dlinhas" = true ]; then
        PS1+="\n"
    else
        PS1+=" "
    fi

    if [ "$smb" = "&" ]; then
        PS1+="& "
    fi

    PS1+="\[$(tput sgr0)\]"

    if [ "$dlinhas" = true ]; then
        PS1+="\n"
    fi

    PS1+="\$ "
}
