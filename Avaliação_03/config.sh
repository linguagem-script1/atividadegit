#!/bin/bash
source func.sh

# Função para escolher e aplicar a customização
choose_customization() {
    echo "Escolha a cor para o prompt:"
    echo "1. Vermelho"
    echo "2. Verde"
    echo "3. Azul"
    read -p "Digite o número da cor desejada: " cor

    case $cor in
        1) color=1 ;; # Vermelho
        2) color=2 ;; # Verde
        3) color=4 ;; # Azul
        *) color=0 ;; # Padrão (branco)
    esac

    read -p "Exibir apenas o nome do usuário? [S/n]: " usuario
    if [[ "$username_only_choice" == "n" || "$username_only_choice" == "N" ]]; then
        usuario=false
    else
        usuario=true
    fi

    read -p "Utilizar prompt em duas linhas? [S/n]: " dlinhas
    if [[ "$two_lines_choice" == "n" || "$two_lines_choice" == "N" ]]; then
        dlinhas=false
    else
        dlinhas=true
    fi

    read -p "Utilizar o símbolo '&' no prompt? [S/n]: " symbol_choice
    if [[ "$symbol_choice" == "n" || "$symbol_choice" == "N" ]]; then
        smb="$"
    else
        smb="&"
    fi

    set_prompt $cor $usuario $dlinhas $smb
    echo "Configuração aplicada."
}
choose_customization
