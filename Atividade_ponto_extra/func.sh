#!/bin/bash

# Função para exibir a i-ésima linha de um arquivo
linha() {
    local linha_num=$1
    local arquivo=$2
    sed -n "${linha_num}p" "$arquivo"
}

# Função para exibir a i-ésima coluna de um arquivo (considerando ':' como separador)
coluna() {
    local col_num=$1
    local arquivo=$2
    cut -d ':' -f "$col_num" "$arquivo"
}
