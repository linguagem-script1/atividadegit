#!/bin/bash

# Função para exibir a i-ésima linha de um arquivo
linha() {
    local linha_num=$1
    local arquivo=$2
    sed -n "${linha_num}p" "$arquivo"
}

# Função para exibir a i-ésima coluna de um arquivo (considerando ':' como separador)
coluna() {
    local col_num=$1
    local arquivo=$2
    cut -d ':' -f "$col_num" "$arquivo"
}

# Pedir ao usuário o nome do arquivo e o tipo de filtro (linha ou coluna)
echo "Digite o nome do arquivo e o tipo de filtro (Exemplo: arquivo.txt L3 ou arquivo.txt C4):"
read entrada_usuario

# Extrair o nome do arquivo e o tipo de filtro da entrada do usuário usando cut
nome_arquivo=$(echo "$entrada_usuario" | cut -d ' ' -f 1)
filtro=$(echo "$entrada_usuario" | cut -d ' ' -f 2)

# Verificar se o arquivo existe
if [ ! -f "$nome_arquivo" ]; then
    echo "Arquivo não encontrado."
    exit 1
fi

# Verificar e separar o tipo de filtro (linha ou coluna) e o número correspondente
tipo=""
numero=""
if [ "${filtro:0:1}" == "L" ] || [ "${filtro:0:1}" == "l" ]; then
    tipo="L"
    numero="${filtro:1}"
elif [ "${filtro:0:1}" == "C" ] || [ "${filtro:0:1}" == "c" ]; then
    tipo="C"
    numero="${filtro:1}"
else
    echo "Filtro inválido. Use L para linha ou C para coluna seguido do número desejado."
    exit 1
fi

# Verificar se o número está dentro do intervalo válido
if ((numero < 1 || numero > 7)); then
    echo "Número inválido. Use um número de 1 a 7 para linha ou coluna."
    exit 1
fi

# Exibir a linha ou coluna conforme especificado pelo usuário
if [ "$tipo" == "L" ]; then
    linha "$numero" "$nome_arquivo"
elif [ "$tipo" == "C" ]; then
    coluna "$numero" "$nome_arquivo"
fi
